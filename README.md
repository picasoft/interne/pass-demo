_Ce dépôt documente comment Picasoft partage ses identifiants entre ses membres. Ceux-ci sont chiffrés à l'aide de pass. Seulement des anciens identifiants sont présents ici. Le dépôt contenant les vrais identifiants n'est pas accessible publiquement._

_Par contre, à partir d'ici le contenu de ce README.md est exactement le même_

# Identifiants de Picasoft

<!-- MarkdownTOC autolink="true" -->

- [Des mots de passe sur Gitlab ?](#des-mots-de-passe-sur-gitlab-)
- [Organisation](#organisation)
- [Utilisation](#utilisation)
    - [Alias pour le store Picasoft](#alias-pour-le-store-picasoft)
    - [Importer les clés publiques des autres membres](#importer-les-cl%C3%A9s-publiques-des-autres-membres)
    - [Ajouter ou mettre à jour un mot de passe](#ajouter-ou-mettre-%C3%A0-jour-un-mot-de-passe)
    - [Ajouter une identité](#ajouter-une-identit%C3%A9)
- [Demande d'accès à une catégorie d'identifiants](#demande-dacc%C3%A8s-%C3%A0-une-cat%C3%A9gorie-didentifiants)
- [Troublesooting](#troublesooting)

<!-- /MarkdownTOC -->

## Des mots de passe sur Gitlab ?

Ici sont stockés les identifiants | mots de passe de Picasoft, chiffrés avec les clés PGP des membres autorisés.

Ils sont, en particulier, tous accessibles par **au moins** deux membres parmi le président, le trésorier et le responsable technique, comme le mentionne le règlement intérieur.

Pour gérer les identifiants, nous utilisons [pass](https://www.passwordstore.org/), qui permet une gestion simple de ces mots de passe en ligne de commande. Il a de nombreux clients (extension de navigateur, application mobile, menu intégré à l'environnement graphique...).

## Organisation

Conformément au règlement intérieur, les identifiants sont répartis en trois catégories :

* Le dossier `Tech` contient les mots de passe liés à la technique (comptes admin, accès root...)
* Le dossier `Asso` contient les mots de passe des moyens de communication, des réseaux sociaux et de la préfecture.
* Le dossier `Tresorerie` contient les identifiants de la banque.

## Utilisation

### Alias pour le store Picasoft

`pass` ne propose par défaut qu'un seul « store », et cette situation est gênante quand vous gérez aussi vos mots de passe personnels.

La variable `$PASSWORD_STORE_DIR` permet de modifier le chemin vers le store concerné, il suffit de la mettre à jour avant de lancer la commande.

Les scripts [picapass](./picapass) et [picamenu](./picamenu) sont des wrappers autour de la commande `pass` et `passmenu` (dépendance : `dmenu`), se contentant d'affecter automatiquement la variable `$PASSWORD_STORE_DIR`.

**Utilisez donc `picapass` et `picamenu` exactement comme `pass` et `passmenu`.**

Exemples :
```bash
# Copie dans le presse papier le mot de passe administrateur du cloud
$ picapass -c Cloud/admin
# Ouvre un menu graphique où l'on peut sélectionner le mot de passe à déchiffre et copier
$ picamenu
```

Pour plus de facilités, ces scripts peuvent être mis dans le `$PATH` ou ajoutés comme des liens symboliques dans un dossier du `$PATH`, par exemple :

```bash
# Méthode 1 : ajout au $PATH, dans ~/.bashrc, ou ~/.zshrc, ou...
export $PATH=${PATH}:/chemin/vers/le/clone/du/repo

# Méthode 2 : liens symboliques
$ cd /chemin/vers/le/clone/du/repo
$ sudo ln -s $(pwd)/picamenu /usr/bin/picamenu
$ sudo ln -s $(pwd)/picapass /usr/bin/picapass
```

Les scripts sont ensuite utilisables depuis un terminal ou un menu, de n'importe où.

Le fonctionnement est ensuite exactement le même que pour `pass`, donc [RTFM !](https://www.passwordstore.org/).

### Importer les clés publiques des autres membres

À chaque fois qu'un mot de passe est créé ou mis à jour, il est chiffré avec les clés publiques des membres autorisé.e.s. Il vous faudra donc importer quelques clés publiques. Pour importer toutes les clés, vous pouvez utiliser le script [picaimport](./picaimport).

### Ajouter ou mettre à jour un mot de passe

Le nommage des dossiers et des fichiers est libre, néanmoins, on respecte en général une convention : les dossiers ont un nom explicite (type `LDAP`, `Mattermost`, `Mail`...) et les fichiers finaux portent le nom de l'**utilisateur** ou de l'**adresse mail** le cas échéant. Sinon, ils portent un nom arbitraire.

On pourra générer un mot de passe et le chiffrer pour l'ensemble des personnes autorisées (voir [Ajouter une identité](#ajouter-une-identit%C3%A9)) du dossier avec la commande `picapass generate`. Exemple :

```bash
# Génère un mot de passe de 25 caractères pour le compte admin du Kanban
$ picapass generate Tech/WeKan/admin -n 25
```

Si en revanche on souhaite insérer un mot de passe déjà existant, ou le mettre à jour :

```bash
# Demande le mot de passe sur l'entrée standard
$ picapass insert Tech/WeKan/admin
```

La gestion avec Git est automatique : dès qu'une modification est faite avec `picapass`, un commit est effectué. Il ne faut pas oublier de `push` ses modifications.

### Ajouter une identité

Chaque dossier de la racine est chiffré pour un **ensemble d'identités**, listées dans le fichier `.gpg-id` de chaque dossier. Lorsque l'on veut rajouter une identité, c'est-à-dire donner accès à une catégorie d'identifiants à une personne :

* On rajoute le fingerprint de sa clé sur une nouvelle ligne du fichier `.gpg-id` ;
* On chiffre les identifiants pour la nouvelle identité.
* Si cette clé n'était pas dans le fichier `gpg_keys.txt`, on le rajoute pour que ce soit plus facile.

Exemple :

```bash
# La clé associée à l'identité quentinduchemin@tuta.io aura accès aux identifiants techniques
$ echo <fingerprint> >> Tech/.gpg-id
# Chiffre les identifiants techniques pour l'ensemble des identités du fichier .gpg-id du dossier Tech.
$ picapass init -p Tech $(cat Tech/.gpg-id)
```

**Attention** : la commande ci-dessus doit être utilisée **sans erreurs**, en particulier oublier `-p` pourrait avoir des conséquences inattendues.

### Note sur l'héritage des clés pour les sous-dossiers

Cela rajoute un peu de complexité, mais notez bien le fonctionnement du chiffrement des identifiants lorsque vous utilisez plusieurs fichiers `.gpg-id` dans des sous-dossiers.

Par exemple, soit l'aborescence suivante :
```
.
├── A
│   └── B
│       └── .gpg-id
├── C
└── .gpg-id
```

Si vous lancez la commande `pass init -p . $(cat .gpg-id)` :
* Les identifiants dans `A` et `C` seront chiffrés avec les clés spécifiées dans `.gpg-id`
* Les identifiants dans `A/B` seront chiffrés **uniquement** avec les clés spécifiée dans `./A/B/.gpg-id` !

En d'autres termes, il n'y a pas d'héritage. La syntaxe de la commande prête à confusion car on dirait vraiment qu'on lui dit de chiffrer tout le dossier Tech avec les clés racines. Mais `pass` intègre cette substitution dans son fonctionnement.

## Demande d'accès à une catégorie d'identifiants

Si vous avez besoin d'accéder à une catégorie d'identifiants, il faut suivre certaines étapes :

1. Création d'une clé PGP. Vous pouvez nous demander de l'aide si vous ne comprenez pas quelque chose. Votre clé privée doit être protégée par un mot de passe suffisamment fort.
2. Certification de votre clé. Faites certifier votre clé par le responsable de la catégorie d'identifiants concernée (président, trésorier ou responsable technique).
3. Chiffrement des identifiants avec votre clé. Le responsable re-chiffre l'ensemble des identifiants concernés avec votre clé puis met à jour le dépôt.
4. Accès aux identifiants. Il suffit de cloner ce dépôt de suivre la procédure (voir [Utilisation](#utilisation)).

## Troublesooting

```
am@am:~/Picasoft/pass$ ./picapass generate Asso/Tetaneutral/mailing-list-technique -n 25
gpg: 1C00A5844EDEB69A75048DAF947DC1E509986B51: skipped: Unusable public key
gpg: [stdin]: encryption failed: Unusable public key
Password encryption aborted.
```

Ce message peu explicite indique qu'il y a un souci avec une des clés publiques requises pour le chiffrement.

Il est possible que cette clé ne soit pas disponible sur votre PC. Il faut donc l'importer (voir [Importer les clés publiques des autres membres](#importer-les-cl%C3%A9s-publiques-des-autres-membres))

Il est également possible qu'elle soit expirée, ou que ses sous-clés soient expirées. Ici, l'id de la clé en question est `1C00A5844EDEB69A75048DAF947DC1E509986B51`. Vous pouvez vérifier les dates d'expiration de la clé et des sous-clés avec `gpg -k --list-options show-unusable-subkey 1C00A5844EDEB69A75048DAF947DC1E509986B51`.

```
am@am:~/Picasoft/pass$ ./picapass generate Asso/Tetaneutral/mailing-list-technique -n 25
gpg: 234072463EE9FABB: There is no assurance this key belongs to the named user
gpg: [stdin]: encryption failed: Unusable public key
Password encryption aborted.
```

Pour corriger cette erreur, il faut modifier le niveau de confiance de la clé concernée (ici, `234072463EE9FABB`)

Pour cela:

1. Vérifier avec le propriétaire de la clé que les empreintes de la clé et des sous-clés sont correctes. (Pour lister les fingerprints: `gpg --fingerprint --with-subkey-fingerprints 234072463EE9FABB`)
2. Marquer la clé comme étant de confiance: `gpg --edit-key 234072463EE9FABB` puis `trust`
